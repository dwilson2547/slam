# Power Delivery

> Barrel jack standards: https://www.cui.com/blog/selecting-appropriate-input-and-output-plugs-for-your-power-adapter

>Primary concern here is that car voltage will vary between 12 and 14.5 volts, while that works fine for a car I'd like something a bit more stable for the sensors to use. I've purchased the following modules to help out: 


1. DC 12v-12v 6a stabalizer 
    https://www.amazon.com/gp/product/B07WFMKMV9/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1
    * The 12v stabalizer will run the USB 3 hub and the Velodyne control box

2. DC 12v-5v 3a step down converter
    https://www.amazon.com/gp/product/B07Y2V1F8V/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1
    * The 5v converters will run the raspberry pi and any other 5v electronics

3. DC 12v to 19v 5a boost converter
    https://www.amazon.com/gp/product/B0B7481QCY/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1
    * The 19v converter will run the router, the router also needs a 5.5x2.5 -  2.5x0.7mm barrel jack adapter