# Cameras

Basler BIP-1000c-dn

In my case, I bought the camera off ebay and had nothing but the quick start manual to go on. From various manuals online I discovered that this particular model doesn't have a physical reset switch, you either have to do it through the web interface or you have to connect to it via i^2c and run some program that has long since been wiped from the internet. I ended up purchasing some of the connectors for the camera's pin breakout and powered it by the first two pins, then i connected an ethernet cable from the camera to my laptop and made sure my laptop network was set to dhcp. Then I started wireshark and selected the ethernet interface and waited for something to appear, sure enough before too long I see a broadcast from the basler camera. I took the ip from that message and put it into my web browser and luckily it was unlocked, giving me complete control over the camera. Someone had set a static ip and gateway that I never could have guessed so I changed it back to DHCP and changed the hostname before hitting commit.

## Wireshark Capture
![Alt text](photos/camera_packet_capture.png)

## Basler Web Interface
![Alt text](photos/basler_web_interface.png)

* The web interface also provided the mac address of the ip cam, so I set a static dhcp assignment in the router for it as well at 192.168.50.30