# A note about access points:

I was under the impressions that all access points came with a built-in client mode, that is not the case, quite the opposite in fact. Despite my build including a wifi capable router, I wanted to have a secondary ap onboard to connect to my home wifi and act as the wan client. A wireless bridge between this data collection device and my home network to provide internet and data-transfer capabilities in the driveway. I think this will make troubleshooting a lot easier, but I need an ap that can connect to my home network as a client.

First, I purchased an outdoor tp-link ap, eap-225v3 outdoor, only to find out it does not support client mode. So I searched for an ap that specifically had client mode and bought a CPE510. This ap promised 300 megabit over 5ghz, so not great by any means, but certainly useable. Once I received the ap, I connected it to my laptop and noticed the link speed was only 100 megabit, sure enough this pos doesn't have a gigabit port, so you won't see the full 300 megabit promised through the lan port. I'm pretty pissed about the whole situation, for now I'll use the 100 meg pos and wait for openwrt to make a build for my eap-225 outdoor. 

# A note about positioning and general considerations

I used what I had available, but if I were to purchase a router for a similar build, I'd try to get one with external antennas so they could be reloacated to the side of the box. I have my router mounted at the top of my kit as that's where it'll have the lease amount of interference from the aluminum plates, but it still isn't a great place to be with the plate to support the lidar right above it. Idelly the antennas would be on the side of the box, uninterrupted by the internal structure. 

# A note about "POE"

Oh yeah, I almost forgot, the CPE510 isn't a pos just because of the 100 meg link speed, it also promises POE without supporting 802.3af. Allow me a moment to explain, "POE" or power over ethernet, is defined by IEEE standard 802.3af. This standard is designed to allow devices to negotiate their power requirements with poe providers so you can have an array of different devices all powered by the same switch. Where the CPE510 screwed up is it claims to support "POE," but it doesn't support the auto negotiation power standard 802.3af. What TP-Link does to provide "power over ethernet" to this device, is provide a mandatory power injector along with the device that adds +24v DC to pins 4 and 5, and adds DC ground to pins 7 and 8. This means the device won't work with any poe switch or power providing device you currently own, only the provided injector.

## Why this matters:
Aside from, at best, dubious claims of poe support, this poses a real problem for my build as the fake poe injector provided with the CPE510 doesn't have a wall wart power supply, it is connected directly to mains voltage and has the power regualtion circuitry built into the device. My build is planned around having all the devices powered by dc-dc converters so it can run off the car's power and I'm not adding an inverter just to run this stupid poe injector. I had already gone on amazon and purchased a TP-Link 802.11af poe injector and a 48v boost converter to run it, my plan was to have that run the access point but now this wouldn't work out. Someone less stubborn (or stupid) would probably just run a normal wan cable out of the enclosure and connect the ap and injector in the driveway when home, or maybe just run a cable as that's much cheaper. Instead, I saw'ed open the poe injector and identified the +24v and ground dc rails, after that I disconnected the high voltage side and soldered wires to the rails. I was able to power the device with a bench top power supply set at 24v dc and when I connected the access point it turned on. Great, now we can power the ap, but I need 24v which I hadn't needed previously, so I had to buy another dc-dc converter and squeeze it in at the back of the power management board along with what was left of the poe injector circuit board. I pretty much had to cut off the high voltage side with a hack saw to make it fit, and I drilled 4 holes in it for stand-offs.

# Photo Album

## EAP-225 outdoor and CPE 510

| | | | |
|-|-|-|-|
| ![](photos/access_points/eap-225_outdoor.jpg) <br> EAP-225 outdoor | ![](photos/access_points/eap-225_outdoor_port.jpg) <br> EAP-225 outdoor port |  ![](photos/access_points/cpe-510-front.jpg) <br> CPE510 | ![](photos/access_points/cpe-510_port.jpg) <br> CPE510 port |

## 802.3af injector
| | | |
|-|-|-|
| ![](photos/access_points/802.3af_injector.jpg) | ![](photos/access_points/802.3af_injector_side1.jpg) | ![](photos/access_points/802.3af_injector_side2.jpg) <br> Wall wart is 48v DC |

## Fake injector

| | | | | 
|-|-|-|-|
| ![](photos/access_points/fake_injector.jpg) | ![](photos/access_points/fake_injector_seam.jpg) <br> Seam I cut along with hacksaw to open the injector up | ![](photos/access_points/fake_injector_power.jpg) <br> Mains power connector | ![](photos/access_points/fake_injector_ports.jpg) <br> Ports |
| ![](photos/access_points/fake_injector_info.jpg) <br> Manufacturer info, actually tells us exactly what it's doing, + on 4 and 5, - on 7 and 8 | ![](photos/access_points/fake_injector_board_minimized.jpg) <br> Minimized board, mains connection removed and wires soldered to dc rails | ![](photos/access_points/fake_injector_board_minimized_back.jpg) <br> Back of minimized board, I ended up removing all high voltage components and sawing the board in half. | ![](photos/access_points/fake_injector_board_parts_removed.jpg) <br> Parts removed |




