

* GPS/IMU (Witmotion WTGAHRS2)
    * [Amazon Link](https://www.amazon.com/dp/B072ZZ83JZ?psc=1&ref=ppx_yo2ov_dt_b_product_details)
    * [Manufacturer Link](https://www.wit-motion.com/inertial-navigation/witmotion-wtgahrs2-10-axis-gps.html)
    * Ros GitHub: https://github.com/ElettraSciComp/witmotion_IMU_ros
    * Ros Wiki: https://wiki.ros.org/witmotion_ros
    * Communication Settings: https://github.com/ElettraSciComp/witmotion_IMU_ros/issues/16
    * | Status | Baud Rate | Frequency, Hz | Polling Interval, ms | Description |
        |--|--|--|--|--|
        | Success |	4800	| 10	| 100 	| Stable |
        | Success |	9600	| 10	| 100 	| Stable |
        | Success |	57600	| 50	| 20 	| Stable |
        | Success |	115200	| 100	| 10 	| Stable |
        | Success |	230400	| 200	| 5 	| Stable |
    * Witmotion Software: https://drive.google.com/drive/folders/1TLutidDBd_tDg5aTXgjvkz63OVt5_8ZZ
    * Datasheet: https://ipfs.elettra.eu/ipfs/QmYr4JWEQs5cVxGUQFevx8YubPq5kTTkacfffbdDGQSM8m/Witmotion%20WTGAHRS1%20Datasheet.pdf
* USB Serial Adapter
    * [Amazon Link](https://www.amazon.com/dp/B078GV9J1B?psc=1&ref=ppx_yo2ov_dt_b_product_details)
    * Driver: https://drive.google.com/file/d/1x87Dvw4-Q97NyiuVP_TudYIRSKu3Uve6/view
* Alternate IMU (bno055)
    * Ros Wiki: https://wiki.ros.org/ros_imu_bno055
* Radar (Continental ARS 408-21)
    * Ros Driver: https://github.com/szenergy/conti_radar_driver
* ROS
    * Recording and Playing back data: https://wiki.ros.org/rosbag/Tutorials/Recording%20and%20playing%20back%20data
    * Creating a catkin workspace: https://wiki.ros.org/catkin/Tutorials/create_a_workspace
    * Tutorials: https://wiki.ros.org/ROS/Tutorials

