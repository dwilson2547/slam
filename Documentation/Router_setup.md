# Router Setup

> These steps will inevitably vary greatly from person to person, my home network is definitely not what most people would have, but hopefully these steps will at least give you enough info to figure it out for your setup.

## Router: Asus RT-N65U
Why? It's what I had laying around. This is a pretty old 802.11b/g/n/ac router/switch/all-in-one thing but it has 4 lan ports on the back which should suit my needs nicely. Also it's pretty slim and if space is more of a concern I can probably shell it easily.

## Network Configuration
I wanted a setup where I could connect my asus router to the rest of my network and still be able to access the asus lan clients, imo this makes things easier becasue the ros nodes and sensors will always have their IPs managed by the asus router and I don't have to change networks on the test bench. To do this, I went through the following steps: 

> Details for my network: <br>
    Main house network: 192.168.0.0/24 <br>
    ROS network: 192.168.50.0/24

My router was already setup with the default configuration so I just modified a few things, if yours is brand new you may have to go through the setup process.

1. Connect asus router wan port to home network
2. Power up router
3. Connect to asus router lan (wifi or ethernet) and open browser to 192.168.0.1
4. Log in and go to lan section
5. Change network to 192.168.50.0/24, in my case I just changed the router ip to 192.168.50.1 and it changed the subnet for me
6. Set any wifi configs you want
7. On host network, open browser to router management page, in my case 192.168.0.1
8. Log in and navigate to router dhcp assignments
9. Add a new static dhcp assignment for asus router mac address and ip address 192.168.0.4
10. Navigate to routing section 
11. Add a new gateway static route with params:
    * Destination Network: 192.168.0.50/24
    * Next Hop: 192.168.0.4
12. Connect ros node to asus router
13. Check asus router homepage for ros node ip address, it was 192.168.50.149
14. From computer on main network, open putty and ssh to 192.168.50.149
15. You should be able to connect and setup is complete

## Assign static IP for ROS node(s)

In the asus router, set static dhcp leases for your ros nodes so they'll always be at the same ip address. I configured my nodes as follows:
| Node | Mac | IP |
| - | - | - |
| ros-node-1 | dc:a6:32:19:83:cf | 192.168.50.10 |
| ros-node-2 | d8:3a:dd:95:92:25 | 192.168.50.20 |
| basler cam | 00:30:53:0d:cf:99 | 192.168.50.30 |