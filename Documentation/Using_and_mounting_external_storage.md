# External Storage

> Since we have to boot from the SD card, I want to store the data from the sensors elsewhere. According to some random article I read, the sd card interface max speed on the rpi 4 is ~35 MB/s whereas USB 3 can hit ~150MB/s. I decided to go with a [500gb ssd](https://www.amazon.com/Inland-Professional-512GB-Internal-Solid/dp/B088KLZKPQ/ref=sr_1_6?crid=21W1HA0593IYS&keywords=inland%2B500gb%2Bssd&qid=1701541980&sprefix=inland%2B500gb%2Bssd%2Caps%2C53&sr=8-6&th=1) from microcenter in a [usb 3 enclosure](https://www.amazon.com/dp/B01MYTZW5R?psc=1&ref=ppx_yo2ov_dt_b_product_details). This is pretty overkill, the connection bandwidth couldn't even saturate a modern hdd drive but a ssd will be more reliable in a bumpy car environment.

If using ntfs you'll need to install the ntfs package:
`sudo apt install ntfs-3g`
If not using ntfs, you'll need to google fstab mount usb drive {format} and figure it out

1. Run `sudo fdisk -l` and look for your disc
    * In my case the drive is /dev/sda, 500gb ssd
    * ![Alt text](photos/fdisk_output.png)
2. Find the UUID of your drive by running `sudo blkid`
    * In my case, we can see /dev/sda1 with UUID="88D4A648D4A637F8", so that's what we'll use
    * ![Alt text](photos/blkid_output.png)
3. Edit your /etc/fstab and add this line:
    * The layout for this file is actually tsv (tab separated value), so you'll want to enter the uuid, then hit tab, then where the drive should be mounted + tab, then drive format + tab, then mount arguments
    * UUID="88D4A648D4A637F8" /mnt/ros-data   ntfs-3g nofail,x-systemd.automount
4. Run `sudo mount -a` to mount the drive, it should be mounted automatically on startup as long as it's plugged in