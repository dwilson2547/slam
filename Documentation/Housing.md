# Box to hold it all

Parts List: 

| Name | Description | Amazon Link | Quantity | 
| - | - | - | - |
| Project Box | waterproof, 300x200x170mm | [link](https://www.amazon.com/gp/product/B096X82P1L/ref=ppx_yo_dt_b_asin_title_o03_s01?ie=UTF8&psc=1) | 1 |
| Front mount pivot | Ballhead camera mount | [link](https://www.amazon.com/dp/B075M336JZ?psc=1&ref=ppx_yo2ov_dt_b_product_details) | 1 (2 total) |
| Rear mount arms | Small camera arm mount | [link](https://www.amazon.com/dp/B07K84X662?psc=1&ref=ppx_yo2ov_dt_b_product_details) | 1 (2 total) |
| Cable passthrough kit | Waterproof cable seals | [link](https://www.amazon.com/gp/product/B08R86V8FX/ref=ppx_yo_dt_b_asin_title_o03_s00?ie=UTF8&psc=1) | 1 |
| Box Mounts | Suction cup camera mounts | [link](https://www.amazon.com/dp/B0BFLTXZQ8?psc=1&ref=ppx_yo2ov_dt_b_product_details) | 4 |
| 12v to 24v boost converter | DC boost converter | [link](https://www.amazon.com/dp/B07XBWHR56?psc=1&ref=ppx_yo2ov_dt_b_product_details) | 1 |
| 12v to 19v boost converter | DC boost converter | [link](https://www.amazon.com/gp/product/B0B7481QCY/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1) | 1 |
| 12v to 12v dc stabilizer | dc voltage stavilizer | [link](https://www.amazon.com/gp/product/B07WFMKMV9/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1) | 2 |
| 12v to 5v converter | dc step down converter | [link]() | 2 |
| DC Barrel Jack | 5.5x2.5mm male | [link](https://www.amazon.com/gp/product/B01CJE0ZLI/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1) | 1 |
| 5.5x2.5mm to 2.5x0.7mm adapter | barrel jack adapter | [link](https://www.amazon.com/gp/product/B0BBB1JG26/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1) | 1 |
| Phoemix Contact 1551697 | Phoenix Connector m12 8pin male -> female panel connector | [link](https://www.mouser.com/ProductDetail/Phoenix-Contact/1551697?qs=247rt3TV2s5JJuODtxjujA%3D%3D&countryCode=US&currencyCode=USD) | 1 |
| USB C connector | USB C connector | [link](https://www.amazon.com/gp/product/B091CRLJM2/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1) | 1 |
| Project box | Smaller project box, ~8x5x3 | [link](https://www.amazon.com/dp/B08N6W6LCK?psc=1&ref=ppx_yo2ov_dt_b_product_details) | 1 |
| 80mm Noctua fan | 12v fan | [link](https://www.amazon.com/dp/B00NEMG9K6?psc=1&ref=ppx_yo2ov_dt_b_product_details) | 1 |
| Carbon air filter | Carbon air filter | [link](https://www.amazon.com/dp/B09F6793ST?psc=1&ref=ppx_yo2ov_dt_b_product_details) | 1 |
| Aluminum plates | Mounting plates | Cut out at home from 1/8 in plate laying around| 3 |
| 1/4 20 all thread, 6 in | all thread | Go to Lowes or Menards | 4 |
| 1/4 20 nut | 1/4 20 nut | Again, Lowes or Menards | Didn't count, get a bag of 100 for like $3 |
| 1/4 20 nylock nut | 1/4 20 nylock nut | Lowes or Menards | 8+, probably get a box of them |
| 1/4 20 coupling nut | 1/4 20 coupling nut | Lowes or Menards | 8 |
| 1/4 20 bolt 1 in | bolt | Lowes or Menards | bag of 100 |
