# Troubleshooting

## Sensor Issues 

### GPS/IMU

* Sensor not acquiring satellites and not returning lat/lon
    * When this happened to me my config was time zone set to -5, gps frequency range to 42 hz, and all outputs enabled. I reset time zone to +8 and gps frequency range to 20 and disabled PDOP and pressure outputs and it seems to be working, suspect i had too much stuff turned on so it couldn't parse the message? no clue.