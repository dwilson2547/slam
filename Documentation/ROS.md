
GPS/IMU: 
https://wiki.ros.org/witmotion_ros

Alternate IMU:
https://wiki.ros.org/ros_imu_bno055

Alternate GPS:
https://github.com/pcdangio/ros-driver_mt3339

Radar:
https://github.com/szenergy/conti_radar_driver

Lidar: 
https://wiki.ros.org/velodyne

Can:
https://wiki.ros.org/libpcan


# Installation on Raspberry PI

I'm using ROS 1 because it seems to be the more compatible with the senors I want to use, and I installed ROS Noetic. This version of ROS requires you to run Ubuntu 20.04 lts desktop/server, which was never meant to run off of a usb drive and making it work seems like a lot of unnecessary effort so I just installed it onto an sd card. I also have a USB 3.0 ssd plugged in to store data onto but it boots from the sd card slot. Once you have ubuntu installed and set up to your liking, here's what I did to get ROS installed: 

1. Enable additional apt repositories (this may already be done but better to be safe)
    1. ```
        sudo add-apt-repository universe
        sudo add-apt-repository restricted
        sudo add-apt-repository multiverse
        ```
2. Follow the ROS official documentation here: https://wiki.ros.org/noetic/Installation/Ubuntu
3. Complete all the steps including installing dependencies
4. At the bottom of the page navigate to [ROS tutorials](https://wiki.ros.org/ROS/Tutorials) -> [Installing and Configuring your ROS Environment](https://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment)
5. Create a workspace according to the instructions, modify your bashrc to point to the new ../devel/setup.bash instead of the one we set in initial setup
    ```bash
    source /opt/ros/noetic/setup.bash # Old config, remove this
    source /home/*user*/catkin_ws/devel/setup.bash # New config, change to this
    ```

# Adding External Storage
* Follow the guide [here](Using_and_mounting_external_storage.md)

# Automotive power delivery concerns
* Addressed [here](Power_Delivery.md)

# GPS/IMU Integration

> Follow the instructions on [GPS Setup](GPS_Setup.md) to configure the sensor prior to ROS integration

We'll be following the instructions provided by the witmotion_ros maintainers<br>
GitHub: https://github.com/ElettraSciComp/witmotion_IMU_ros<br>
ROS Wiki: https://wiki.ros.org/witmotion_ros<br>

1. Run `sudo apt install ros-noetic-witmotion-ros`
2. Install dependencies: `sudo apt-get install libqt5serialport5-dev`
3. Build the project:
    ```
    cd catkin_ws
    git clone --recursive https://github.com/ElettraSciComp/witmotion_IMU_ros.git src/witmotion_ros
    catkin_make
    ```
4. Edit the default config:
    1. run `rosed witmotion_ros config.yaml`
    2. change baud_rate to 115200
    3. change polling_interval to 10
    4. scroll down and set gps_publisher -> enabled to true instead of false. If it is already true it's fine
    5. save your changes
    6. Optionally see my config [here](downloads/GPS_IMU_config.yaml) to compare to yours
5. Testing:
    1. Open a tmux session and run roscore, once started leave the session
    2. Open another tmux session and run roslaunch witmotion_ros witmotion.launch, once started leave the session
    3. run rostopic list to see the available topics
    4. run rostopic echo {topicname} to view the messages for a particular topic
    5. run rosbag record -a to start recording the data from all topics at the current directory, press control + c to stop recording