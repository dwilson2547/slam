
GPS: 
WTGAHRS2 MPU9250 10-axis Accelerometer+Gyroscope+Angle(XY 0.05° Accuracy)+Magnetometer+Air Pressure+Latitude+Longitude+Ground Speed, Navigation System
https://www.amazon.com/gp/product/B072ZZ83JZ/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1

IMU:
https://www.amazon.com/Adafruit-9-DOF-Accel-Breakout-Board/dp/B06XH5Y6DC/ref=sr_1_5?crid=1CFTGD19AZ6UW&keywords=bno055&qid=1701152976&s=electronics&sprefix=bno055%2Celectronics%2C53&sr=1-5

USB-Serial converter: 
https://www.amazon.com/USB-Convert-TTL-Multifunctional-Functions/dp/B01CNW061U?psc=1&pd_rd_w=F6cQ8&content-id=amzn1.sym.3c1bdb31-a20d-42d0-a8cf-ddadf63cd1a8&pf_rd_p=3c1bdb31-a20d-42d0-a8cf-ddadf63cd1a8&pf_rd_r=DJVQXPBXFMD16GB8CSET&pd_rd_wg=Qg85J&pd_rd_r=a5b06a06-9e5a-4f40-b878-56b61ccf4502&ref_=sspa_dk_detail_1

Lidar:
Velodyne VLP-32 

Camera:
https://www.amazon.com/gp/product/B09BCF6TGK/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&th=1 <br>
Lens:
https://www.amazon.com/dp/B0CM26W8FG?psc=1&ref=ppx_yo2ov_dt_b_product_details

Data Collector:


Usb Can converter:
https://www.amazon.com/Grid-Connect-GC-CAN-USB-CAN-Adapter/dp/B078VRJS5X/ref=sr_1_4?keywords=canbus+usb&qid=1701154727&sr=8-4&ufe=app_do%3Aamzn1.fos.304cacc1-b508-45fb-a37f-a2c47c48c32f

