# GPS Setup

1. Connect wires to serial usb adapter
    a. Sensor vcc to usb 5v
    b. Sensor ground to usb ground
    c. Sensor Rx to usb Tx
    D. Sensor Tx to usb Rx
2. Download and install [witmotion windows software](https://drive.google.com/drive/folders/1TLutidDBd_tDg5aTXgjvkz63OVt5_8ZZ)
3. Install [usb serial driver](https://drive.google.com/file/d/1x87Dvw4-Q97NyiuVP_TudYIRSKu3Uve6/view)
4. Open witmotion software and connect to sensor
    1. ![](photos/witmotion_software.png)
5. Open config for your sensor
    1. Set baud rate to 115200
    2. Set frequency to 100
    3. enable all outputs (Update: Ended up disabling port, PDOP, Quaternion, and Pressure)
    4. this would be a good time to run some calibrations too
    5. Credit: https://github.com/ElettraSciComp/witmotion_IMU_ros/issues/16
    6. ![Alt text](photos/gps_config_screen.png)
